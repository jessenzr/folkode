﻿Para rodar o programa e necessario ter o python instalado.
Faca o download no site http://python.org

ou use o link abaixo para baixar a versão 64 bits para windows:

https://www.python.org/ftp/python/3.5.0/python-3.5.0-amd64.exe

IMPORTANTE: Nao se esqueca de marcar a opcao 'Add Python to PATH'



Após baixar, abra o cmd, vá até a pasta do folkode e rode

'python src/folkode.py codigo-fonte.fk', ou utilize o teste.py na
raiz do projeto para rodar o arquivo exemplo.fk.