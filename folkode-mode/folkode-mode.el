;; folkcode mode

(setq folkcode-keywords '("begin"
                          "end"
                          "procedure"
                          "if"
                          "else"
                          "for"
                          "while"
                          "program"
                          "const"
                          "var"
                          "do"
                          "then"
                          ))
(setq folkcode-types '("real" "integer" "string"))
(setq folkcode-functions '("print" "read"))
(setq folkcode-commentaries-single-line '("c "))

(setq folkcode-keywords-regexp (regexp-opt folkcode-keywords 'words))
(setq folkcode-types-regexp (regexp-opt folkcode-types 'words))
(setq folkcode-functions-regexp (regexp-opt folkcode-functions 'words))
(setq folkcode-functions-regexp (regexp-opt folkcode-functions 'words))

(setq folkcode--syntax-table
      `(
        (,"\\(c\\[\\].*\\)" . font-lock-comment-face)
        (,folkcode-keywords-regexp . font-lock-keyword-face)
        (,folkcode-types-regexp . font-lock-type-face)
        (,folkcode-functions-regexp . font-lock-builtin-face)
        ))

(setq folkcode-mode-syntax-table
  (let ((st (make-syntax-table)))
    (modify-syntax-entry ?\\ "."   st)
    (modify-syntax-entry ?\( "()1" st)
    (modify-syntax-entry ?\) ")(4" st)
    ;; This used to use comment-syntax `b'.  But the only document I could
    ;; find about the syntax of Pascal's comments said that (* ... } is
    ;; a valid comment, just as { ... *) or (* ... *) or { ... }.
    (modify-syntax-entry ?* ". 23" st)
    ;; Allow //...\n comments as accepted by Free Pascal (bug#13585).
    (modify-syntax-entry ?\n "> c" st)
    (modify-syntax-entry ?{ "<"    st)
    (modify-syntax-entry ?} ">"    st)
    (modify-syntax-entry ?+ "."    st)
    (modify-syntax-entry ?- "."    st)
    (modify-syntax-entry ?= "."    st)
    (modify-syntax-entry ?% "."    st)
    (modify-syntax-entry ?< "."    st)
    (modify-syntax-entry ?> "."    st)
    (modify-syntax-entry ?& "."    st)
    (modify-syntax-entry ?| "."    st)
    (modify-syntax-entry ?_ "w"    st)
    (modify-syntax-entry ?\' "\""  st)
    st))

(defconst folkcode-syntax-propertize-function
  (syntax-propertize-rules
   ;; Use the `b' style of comments to avoid interference with the -- ... --
   ;; comments recognized when `sgml-specials' includes ?-.
  ;; FIXME: beware of <!--> blabla <!--> !!
   ("\\(#\\)-->" (1 "< b"))
   ("<--\\(#\\)" (1 "> b"))

    ;; Double quotes outside of tags should not introduce strings.
    ;; Be careful to call `syntax-ppss' on a position before the one we're
    ;; going to change, so as not to need to flush the data we just computed.
   ("\"" (0 (if (prog1 (zerop (car (syntax-ppss (match-beginning 0))))
                  (goto-char (match-end 0)))
                (string-to-syntax "."))))
   ("'" (0 (if (prog1 (zerop (car (syntax-ppss (match-beginning 0))))
                  (goto-char (match-end 0)))
                (string-to-syntax "."))))
   ))


;;;###autoload
(define-derived-mode folkcode-mode pascal-mode "Folkcode"
  :syntax-table folkcode-mode-syntax-table
  (setq-local syntax-propertize-function folkcode-syntax-propertize-function)
  (setq-local font-lock-defaults '((folkcode--syntax-table))))

(provide 'folkcode-mode)
