import re
from token_table import (TOKEN_TABLE,
                         TOKEN_IDENT,
                         PARSING,
                         REGRA,
                         ARGUMENTOS,
                         TIPOS,
                         TIPOS_VALOR,
                         TIPOS_INDEX)


class Folkode():
    def __init__(self, arquivo):
        self.__init_variaveis__()
        self.__ler_programa__(arquivo)

    def __init_variaveis__(self):
        self.tabela = {
            'letra': {
                'validate': re.compile('^[a-z]'),
                'sequencia': re.compile('[a-zA-Z_0-9]+')
            },
            'whitespace': re.compile('^\s+'),

            'simbolo': re.compile('(>=|=|>|<>|<=|<|\+|;|:=' +
                                  '|:|\/|\.|,|\*|\)|\(|-)'),

            'comentario-multi': {
                'validate': re.compile('^#'),
                'sequencia': re.compile('#-->\n*(.*?)\n*<--#', re.DOTALL),
            },

            'comentario-single': {
                'validate': re.compile("^c\["),
                'sequencia': re.compile("(c\[\]).*"),
            },

            'string': {
                'validate': re.compile('^"'),
                'sequencia': re.compile('"(.*?)"')
            },

            'real': {
                'validate': re.compile('^\d+\.'),
                'sequencia': re.compile('\d+\.\d+')
            },

            'inteiro': {
                'validate': re.compile('^\d'),
                'sequencia': re.compile('\d+')
            },
        }

        self.tokens = []
        self.linha_atual = 1
        self.caractere_atual = 0
        self.integer_limit = 999999
        self.real_limit = 999999.999
        self.pilha = [40, 42]
        self.procedures = []
        self.contando_argumentos = []
        self.contando_argumentos_chamada = False
        self.verbose = True
        self.variaveis_declaradas = []
        self.declarando_variavel = False
        self.verificando_tipo = None
        self.procedures_declaradas = []
        self.procedures_args = {}
        self.declarando_procedure = False
        self.setando_variaveis = False
        self.variaveis_setadas = []
        self.verifica_tipo_var = 0
        self.variaveis_procedures = []
        self.declarando_variavel_procedure = False
        self.begin = False
        self.end = False

        self.ignorar_ate_linha = 0

    def __check_integer_limit__(self, number):
        if number > self.integer_limit or number < self.integer_limit * -1:
            return False

        return True

    def __check_real_limit__(self, number):
        if (number > self.integer_limit or number < self.integer_limit * -1):
            return False

        if len(str(number).split('.')[1]) > 3:
            return False

        return True

    def __show_error__(self, error):
        print('Um bom viking sempre acerta o código na primeira tentativa:')
        print('- Linha', self.linha_atual, '-', error)
        quit()
        pass

    def __ler_programa__(self, arquivo):
        f = open(arquivo, 'r')

        content = f.readlines()

        for line in content:
            self.caractere_atual = 0

            if self.linha_atual <= self.ignorar_ate_linha:
                self.linha_atual += 1
                continue

            while self.caractere_atual < len(line):
                self.caractere_atual += (
                    self.__get_token__(line[self.caractere_atual:],
                                       "\n".join
                                       (content[self.linha_atual - 1:])))

            self.linha_atual += 1

        self.classifica('$', TOKEN_TABLE['$'], 'final de arquivo')

    def __get_token__(self, line_text, content):
        if self.tabela['comentario-multi']['validate'].match(line_text):
            result = self.tabela['comentario-multi']['sequencia']\
                         .search(content)

            self.match(result, group=1, classifica=False, comentario=True)

            num_linhas = result.group(1).count("\n") - 1
            self.ignorar_ate_linha = self.linha_atual + num_linhas
            return len(line_text)

        if self.tabela['string']['validate'].match(line_text):
            return self.match(self.tabela['string']
                              ['sequencia'].match(line_text),
                              group=1,
                              classifica=True,
                              codigo=TOKEN_TABLE['literal'])

        if self.tabela['comentario-single']['validate'].match(line_text):
            return self.match(self.tabela['comentario-single']
                              ['sequencia'].match(line_text),
                              group=1,
                              classifica=False)

        if self.tabela['letra']['validate'].match(line_text):
            return self.match(self.tabela['letra']['sequencia']
                              .match(line_text))

        if self.tabela['real']['validate'].match(line_text):
            return self.match(self.tabela['real']['sequencia']
                              .match(line_text), codigo=TOKEN_TABLE['nreal'])

        if self.tabela['inteiro']['validate'].match(line_text):
            return self.match(self.tabela['inteiro']['sequencia']
                              .match(line_text), codigo=TOKEN_TABLE['nint'])

        if self.tabela['whitespace'].match(line_text):
            return self.match(self.tabela['whitespace'].match(line_text),
                              classifica=False)

        if self.tabela['simbolo'].match(line_text):
            return self.match(self.tabela['simbolo'].match(line_text))

        self.__show_error__('Erro encontrado em "%s"' % line_text.replace("\n", ""))

        return len(line_text)

    def get_token(self, n = 0):
        n += 1
        tokens_count = len(self.tokens)
        try:
            return self.tokens[tokens_count - n]
        except:
            return None

    def get_prev_token(self, n = 0):
        n += 2
        tokens_count = len(self.tokens)
        try:
            return self.tokens[tokens_count - n]
        except:
            return None

    def classifica(self, token, codigo, identificacao='nope'):
        token_dict = {
            'token': token,
            'line': self.linha_atual,
            'codigo': codigo,
            'identificacao': TOKEN_IDENT[codigo],
            'nargs': 0,
            'tipo': '',
        }

        self.tokens.append(token_dict)

    def match(self,
              match,
              classifica=True,
              group=0,
              codigo=None,
              comentario=False):

        if comentario and not match:
            self.__show_error__('Comentário de várias linhas não fechado')

        if classifica:
            if codigo == TOKEN_TABLE['literal'] and not match:
                self.__show_error__('String não fechada')

            token = match.group(group)
            token_aux = token

            if not codigo:
                if token in TOKEN_TABLE.keys():
                    codigo = TOKEN_TABLE[token]
                else:
                    codigo = TOKEN_TABLE['ident']
                    token_aux = 'ident'

            if codigo == TOKEN_TABLE['nint']:
                if not self.__check_integer_limit__(int(token)):
                    self.__show_error__('Limite do integer ultrapassado')
                token_aux = 'nint'

            if codigo == TOKEN_TABLE['nreal']:
                if not self.__check_real_limit__(float(token)):
                    self.__show_error__('Limite do real ultrapassado')
                token_aux = 'nreal'

            if codigo == TOKEN_TABLE['literal']:
                 token_aux = 'literal'

            self.classifica(token, codigo)
            self.analise_sintatico(token_aux, token)

        return len(match.group(0))

    def print_tokens(self):
        print("codigo" + "	" +
              "token" + "	" +
              "linha" + "	" +
              "identificacao")

        for token in self.tokens:
            print(str(token['codigo']) + "	" +
                  token['token'] + "	" +
                  str(token['line']) + "	" +
                  token['identificacao'])

    def verificacao_sintatico(self):
        if (self.pilha.pop() == TOKEN_TABLE['$']):
            print('analise sintatica concluida com exito.')
        else:
            self.__show_error__("Um erro sintático foi encontrado.")

    def print_tokens_pretty(self):
        from prettytable import PrettyTable

        table = PrettyTable(['codigo',
                             'token',
                             'linha',
                             'identificacao',
                             'tipo',
                             'nargs'])

        table.align['codigo'] = 'l'
        table.align['token'] = 'l'
        table.align['linha'] = 'r'
        table.align['identificacao'] = 'l'
        table.align['nargs'] = 'r'
        table.align['tipo'] = 'l'

        for token in self.tokens:
            table.add_row([str(token['codigo']),
                           token['token'],
                           str(token['line']),
                           token['identificacao'],
                           token['tipo'],
                           token['nargs']])

        print(table)

    def analise_sintatico(self, token, orig):
        # token, linha e conteudo
        x = self.pilha.pop()
        a = TOKEN_TABLE[token]

        if self.verbose:
            print('------------')
        while (x != TOKEN_TABLE['$']):
            self.analise_semantica(token, x, orig)
            if self.verbose:
                print('estado atual da pilha: ', self.pilha)
                print('x: ', x, ' a: ', a)

            if (x == TOKEN_TABLE['î']):
                x = self.pilha.pop()
            else:
                if (x in TOKEN_TABLE.values()):
                    if (x == a):
                        break
                    else:
                        self.__show_error__('Um erro sintático foi encontrado.')
                else:
                    if (x, a) in PARSING.keys() and PARSING[x, a]:
                        regra = REGRA[PARSING[x, a]]
                        if self.verbose:
                            print('regra utilizada: ', PARSING[x, a])
                            print('------------')

                        if isinstance(regra, tuple):
                            for codigo in reversed(regra):
                                self.pilha.append(codigo)
                        else:
                            self.pilha.append(regra)

                        x = self.pilha.pop()
                    else:
                        self.__show_error__("Um erro sintático foi encontrado.")

    def get_procedure(self, agulha):
        for key, token in enumerate(self.tokens):
            if token['codigo'] == TOKEN_TABLE['procedure']:
                procedure = self.tokens[key + 1]
                if procedure['token'] == agulha:
                    return procedure

        return None

    def processa_procedures(self, codigo):
        token = self.tokens[-1]

        if codigo in ARGUMENTOS and self.contando_argumentos:
            self.contando_argumentos['nargs'] += 1

        if TOKEN_TABLE['ident'] == codigo:
            prev = self.get_prev_token()
            if prev['codigo'] == TOKEN_TABLE['procedure']:
                self.contando_argumentos = self.tokens[-1]

        if codigo == TOKEN_TABLE[')'] and self.contando_argumentos:
            if self.contando_argumentos_chamada:
                procedure = self.get_procedure(self.
                                               contando_argumentos['token'])

                if self.contando_argumentos['nargs'] != procedure['nargs']:
                    self._erro_semantico("Número de argumentos diferente")

                self.contando_argumentos_chamada = False
            self.contando_argumentos = []

        if codigo == TOKEN_TABLE['('] and not self.contando_argumentos:
            prev = self.get_prev_token()
            # se token anterior nao for reservado
            if prev['token'] not in TOKEN_TABLE.keys():
                self.contando_argumentos = prev
                self.contando_argumentos_chamada = True

    def get_tipo_variavel(self, var):
        for token in self.tokens:
            if token['token'] == var and token['tipo']:
                return token['tipo']

    def processa_variaveis(self, x):
        if x == TOKEN_TABLE['var']:
            self.setando_variaveis = True

        if x == TOKEN_TABLE['ident'] and self.setando_variaveis:
            self.variaveis_setadas.append(self.get_token()['token'])

        if self.setando_variaveis and x in TIPOS:
            self.setando_variaveis = False
            for token in self.tokens:
                if token['token'] in self.variaveis_setadas:
                    token['tipo'] = x

            self.variaveis_setadas = []

        if self.verifica_tipo_var and x in TIPOS_VALOR:
            if x == TOKEN_TABLE['ident']:
                tipo = self.get_tipo_variavel(self.get_token()['token'])
            else:
                tipo = x

            tipo = TIPOS_INDEX[tipo]

            if self.verifica_tipo_var != tipo:
                self._erro_semantico('Tipo setado difere do da variável')

        if x == TOKEN_TABLE[':=']:
            var = self.get_prev_token()
            self.verifica_tipo_var = self.get_tipo_variavel(var['token'])


    def analise_semantica(self, token, x, orig):
        if TOKEN_TABLE['nint'] == x:
            self.divizao_0(orig)

        self.processa_procedures(x)
        self.processa_variaveis(x)

        if x in (TOKEN_TABLE['ident'], TOKEN_TABLE['var'], TOKEN_TABLE[':']):
            self.variaveis_mesmo_nome(orig, x)

        if x in (TOKEN_TABLE['ident'],
                 TOKEN_TABLE['procedure'],
                 TOKEN_TABLE['('],
                 TOKEN_TABLE[')']):
            self.monta_lista_procedures(orig, x)

        if x == TOKEN_TABLE['begin']:
            self.begin = True

        if x == TOKEN_TABLE['end']:
            self.begin = False
            self.end = True
        
        if x == TOKEN_TABLE[';']:
            if self.end:
                self.variaveis_procedures = []
                self.end = False

        if x == TOKEN_TABLE['ident']:
            self.uso_de_variavel_nao_declarada(orig)

    def _erro_semantico(self, error):
        self.__show_error__("Erro semantico: " + error)

    def divizao_0(self, token):
        token = float(token)

        if token == 0:
            tokens_count = len(self.tokens)
            prev = self.tokens[tokens_count - 2]['codigo']
            if prev == TOKEN_TABLE['/']:
                self._erro_semantico("Divisão por 0")

    def variaveis_mesmo_nome(self, token, x):
        if x == TOKEN_TABLE['var']:
            self.declarando_variavel = True

        elif x == TOKEN_TABLE['ident']:
            if self.declarando_variavel:
                if token in self.variaveis_declaradas:
                    self._erro_semantico("Variável '" + token
                                         + "' foi declarada mais de uma vez")
                else:
                    self.variaveis_declaradas.append(token)

        elif x == TOKEN_TABLE[':']:
            self.declarando_variavel = False

    def monta_lista_procedures(self, token, x):
        if x == TOKEN_TABLE['procedure']:
            self.declarando_procedure = True
            self.declarando_variavel_procedure = True
            
        elif x == TOKEN_TABLE['ident']:
            if self.declarando_procedure:
                if token not in self.procedures_declaradas:
                    self.procedures_declaradas.append(token)
            else:
                if self.declarando_variavel_procedure:
                    if token not in self.variaveis_procedures:
                        self.variaveis_procedures.append(token)

        elif x == TOKEN_TABLE['(']:
            self.declarando_procedure = False

        elif x == TOKEN_TABLE[')']:
            self.declarando_variavel_procedure = False

    def uso_de_variavel_nao_declarada(self, token):
        if self.begin:
            if token not in self.variaveis_declaradas and token not in self.variaveis_procedures and token not in self.procedures_declaradas:
                self._erro_semantico("Variável ou procedure '" + token + "' não foi declarada.")

if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser(description='Folkode compilador')

    parser.add_argument('arquivo', metavar='arquivo',
                        type=str, nargs=1,
                        help='Arquivo a ser compilado')

    parser.add_argument('--bonito', action='store_true',
                        help='Directory where the PKGBUILDs will ' +
                        'be generated. The current directory is the default')

    args = parser.parse_args()

    arquivos = args.arquivo

    for arquivo in arquivos:
        folk = Folkode(arquivo)

        if folk.verbose:
            if args.bonito:
                folk.print_tokens_pretty()
            else:
                folk.print_tokens()

        folk.verificacao_sintatico()
